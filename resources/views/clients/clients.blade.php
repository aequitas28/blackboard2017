@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Clients
                    <a href="/clients/create" class="btn btn-success" style="float:right;"> <span class="glyphicon glyphicon-plus"> Add New </span> </a>
                </div>

                @if(session()->has('success'))
                    <div class="alert alert-success" role="alert">
                        {!! session()->get('success') !!}
                    </div>
                @endif

                @if(session()->has('error'))
                    <div class="alert alert-danger" role="alert">
                        {!! session()->get('error') !!}
                    </div>
                @endif

                @if(session()->has('notice'))
                    <div class="alert alert-info" role="alert">
                        {!! session()->get('notice') !!}
                    </div>
                @endif

                <div class="card-body">
                    
                    <table class="table table-hover">
                        <thead class="thead-dark">
                            <tr>
                            <th>Name</th>
                            <th>Creator</th>
                            <th>Created</th>
                            <th>Status</th>
                            <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($clients as $client)

                                <tr>
                                    <td scope="row">{{$client->name}}</td>
                                    <td>{{$client->creator}}</td>
                                    <td>{{$client->created_at}}</td>
                                    <td>
                                        @if($client->deleted_at == "")
                                            <span class="badge badge-success">Active</span>
                                        @else
                                            <span class="badge badge-danger">Inactive</span>
                                        @endif
                                    </td>
                                    <td>
                                        
                                        <a href="/clients/view/{{$client->id}}" class="btn btn-sm btn-primary">View</a>
                                    
                                        <a href="/clients/edit/{{$client->id}}" class="btn btn-sm btn-secondary">Edit</a>
                                        <a 
                                        @if($client->deleted_at == "")
                                            class="btn btn-sm btn-danger" 
                                            href="/clients/delete/{{$client->id}}"
                                        @else 
                                            class="btn btn-sm btn-danger disabled"    
                                        @endif
                                        >Delete</a>
                                        
                                    </td>
                                </tr>
                                    
                            @endforeach
                            
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
