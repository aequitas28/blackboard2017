@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Client | View</div>

                <div class="card-body">
                    
                    @if($client == null)

                        <div class="alert alert-danger" role="alert">
                            Client does not exists or you do not have access to this information
                        </div>

                    @else

                        <table class="table">
                            <tr>
                                <th> Name </th>
                                <td> {{$client->name}} </td>
                            </tr>
                            <tr>
                                <th> Creator </th>
                                <td> {{$client->creator_first}} {{$client->creator_last}} </td>
                            </tr>
                            <tr>
                                <th> Created </th>
                                <td> {{$client->created_at}} </td>
                            </tr>
                            <tr>
                                <th> Last updated </th>
                                <td> {{$client->updated_at}} </td>
                            </tr>
                            <tr>
                                <th> Status </th>
                                <td> 
                                    @if($client->deleted_at == "")
                                        <span class="badge badge-success">Active</span>
                                    @else
                                        <span class="badge badge-danger">Inactive</span>
                                    @endif
                                </td>
                            </tr>
                        </table>

                        <a href="/clients/" class="btn btn-sm btn-primary">Back</a>
                        <a href="/clients/edit/{{$client->id}}" class="btn btn-sm btn-secondary">Edit</a>
                        <a 
                        @if($client->deleted_at == "")
                            class="btn btn-sm btn-danger" 
                            href="/clients/delete/{{$client->id}}"
                        @else 
                            class="btn btn-sm btn-danger disabled"    
                        @endif
                        >Delete</a>

                    @endif    

                    
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
