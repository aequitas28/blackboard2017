@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Client | New</div>

                <div class="card-body">
                    
                    <form method="post" action="/clients">
                        {{csrf_field()}}

                        <div class="form-group">
                            <label for="name">Client name</label>
                            <input type="text" class="form-control" name="name" max="255" placeholder="Enter client name">
                        </div>
                        <a href="/clients/" class="btn btn-secondary"> Cancel </a>
                        <button type="submit" class="btn btn-primary"> Create </button>
                    </form>    
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
