@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Client | Edit</div>

                <div class="card-body">
                    
                    <form method="post" action="/clients/{{$client->id}}">
                        {{csrf_field()}}

                        <div class="form-group">
                            <label for="name">Client name</label>
                            <input type="text" class="form-control" name="name" max="255" value="{{$client->name}}" placeholder="Enter client name">
                        </div>
                        <a href="{{ URL::previous() }}" class="btn btn-secondary"> Cancel </a>
                        <button type="submit" class="btn btn-primary"> Submit </button>
                    </form>                 
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
