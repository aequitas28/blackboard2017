<?php

namespace Blackboard\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = DB::table('clients')
                        ->leftJoin('users', 'users.id', '=', 'clients.created_user_id')
                        ->select('clients.id','clients.name','clients.created_at','clients.deleted_at','users.first_name as creator')
                        ->where('clients.created_user_id', '=', Auth::user()->id)
                        ->get();

        return view('clients/clients', ['clients' => $clients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clients/clientsnew');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $id =  DB::table('clients')->insertGetId(
            ['name' => $request->input('name'), 'created_at'=>date('Y-m-d H:i:s'), 'created_user_id' => Auth::user()->id]
        );

        if($id != null)
            return redirect('clients')->with('success',"<strong>Successful!</success> New client captured successfully. <a href='/clients/view/".$id."'> View details.</a> </div>");
        else
            return redirect('clients')->with('error',"<strong>Unsuccessful!</success> New client creation failed. </div>");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = DB::table('clients')
                        ->leftJoin('users', 'users.id', '=', 'clients.created_user_id')
                        ->select('clients.id','clients.name','clients.created_at','clients.updated_at','clients.deleted_at','users.first_name as creator_first','users.last_name as creator_last')
                        ->where('clients.created_user_id', '=', Auth::user()->id)
                        ->where('clients.id', '=',$id)
                        ->first();


        return view('clients/clientsview',['client' => $client]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = DB::table('clients')
                        ->leftJoin('users', 'users.id', '=', 'clients.created_user_id')
                        ->select('clients.id','clients.name')
                        ->where('clients.created_user_id', '=', Auth::user()->id)
                        ->where('clients.id', '=',$id)
                        ->first();

        return view('clients/clientsedit',['client'=>$client]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('clients')
            ->where('id', $id)
            ->update(['name' => $request->input('name')]);

        return redirect('clients')->with('notice',"<strong>Notice!</success> Client updated successfully. <a href='/clients/view/".$id."'> View details.</a> </div>");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('clients')
            ->where('id', $id)
            ->update(['deleted_at' => date('Y-m-d H:i:s')]);

        return redirect('clients')->with('notice',"<strong>Notice!</success> Client deleted successfully. <a href='/clients/view/".$id."'> View details.</a> </div>");    
    }
}
