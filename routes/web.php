<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/profile','UserController@index')->name('auth/profile');
Route::post('/profile/0','UserController@update')->name('auth/profile');

Route::get('/clients','ClientController@index')->name('clients/clients');
Route::get('/clients/create','ClientController@create')->name('clients/clientsnew');
Route::post('/clients/{id}','ClientController@update')->name('clients/clients');
Route::post('/clients','ClientController@store')->name('clients/clients');
Route::get('/clients/view/{id}','ClientController@show')->name('clients/clientsview');
Route::get('/clients/edit/{id}','ClientController@edit')->name('clients/clientsedit');
Route::get('/clients/delete/{id}','ClientController@destroy')->name('clients/clientsdelete');
Route::get('/home', 'ClientController@index')->name('clients/clients');


